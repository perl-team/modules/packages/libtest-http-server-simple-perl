libtest-http-server-simple-perl (0.11-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Antonio Radici from Uploaders. Thanks for your work!
  * Remove AGOSTINI Yves from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Contact, Name.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 12:16:21 +0100

libtest-http-server-simple-perl (0.11-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:41:11 +0200

libtest-http-server-simple-perl (0.11-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Rewrite libtest-simple-perl build dependency.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Use dh(1) and debhelper compat level 9.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.7.
  * Switch to source format "3.0 (quilt)".

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Feb 2016 20:07:45 +0100

libtest-http-server-simple-perl (0.10-1) unstable; urgency=low

  [ Antonio Radici ]
  * New upstream release
  * debian/control:
    + added myself to the uploaders
    + Standards-Version bumped to 3.8.0
  * debian/copyright:
    + added GPL-1 and Perl licenses extracts
    + removed debian perl group as copyright holder, because it is
      not a legal entity

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * Add /me to Uploaders.

 -- Antonio Radici <antonio@dyne.org>  Fri, 20 Feb 2009 19:23:07 +0000

libtest-http-server-simple-perl (0.09-1) unstable; urgency=low

  * Initial Release (Closes: #476938)

 -- AGOSTINI Yves <agostini@univ-metz.fr>  Mon, 12 May 2008 09:21:05 +0200
